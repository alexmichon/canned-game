// rotation
if keyboard_check(vk_shift){
    self.speed = 0;
    
    //right
    if keyboard_check(vk_right) and not keyboard_check(vk_up) and not keyboard_check(vk_down){
        dir = 0;
        image_index = 0;
    }
    
    //up
    if keyboard_check(vk_up)
    {
        if not keyboard_check(vk_right) and not keyboard_check(vk_left) {
            image_index = 2;
            dir = 90;
        }
        if keyboard_check(vk_right) {
            dir = 45;
            image_index = 1;
        }
        
        if keyboard_check(vk_left) {
            dir = 135;
            image_index = 3;
        }
    }
    
    //left
    if keyboard_check(vk_left) and not keyboard_check(vk_up) and not keyboard_check(vk_down){
        image_index = 4;
        dir = 180;
    }
    
    
    //down
    if keyboard_check(vk_down){
        if not keyboard_check(vk_right) and not keyboard_check(vk_left){
            image_index = 6;
            dir = 270;
        }
        if keyboard_check(vk_right){
            image_index = 7;
            dir = 315;
        }
        if keyboard_check(vk_left){
            dir = 225;
            image_index = 5;
        }  
    }
}

//movement
else{
    //right
    if keyboard_check(vk_right) and not keyboard_check(vk_up) and not keyboard_check(vk_down){
        dir = 0;
        image_index = 0;
        hspeed = 6.25;
    }
    
    //up
    if keyboard_check(vk_up){
        if not keyboard_check(vk_right) and not keyboard_check(vk_left) {
            image_index = 2;
            dir = 90;
            vspeed = -6.25;
        }
        if keyboard_check(vk_right) {
            dir = 45;
            image_index = 1;
            vspeed = -sqrt(19.53125)
            hspeed = sqrt(19.53125)
        }
        
        if keyboard_check(vk_left) {
            dir = 135;
            image_index = 3;
            vspeed = -sqrt(19.53125)
            hspeed = -sqrt(19.53125)
        }
    }
    
    //left
    if keyboard_check(vk_left) and not keyboard_check(vk_up) and not keyboard_check(vk_down){
        image_index = 4;
        hspeed = -6.25;
        dir = 180;
    }
    
    
    //down
    if keyboard_check(vk_down){
        if not keyboard_check(vk_right) and not keyboard_check(vk_left){
            image_index = 6;
            dir = 270;
            vspeed = 6.25;
        }
        if keyboard_check(vk_right){
            image_index = 7;
            dir = 315;
            vspeed = sqrt(19.53125)
            hspeed = sqrt(19.53125)
        }
        if keyboard_check(vk_left){
            dir = 225;
            image_index = 5;
            vspeed = sqrt(19.53125)
            hspeed = -sqrt(19.53125)
        }  
    }
}

// released
if keyboard_check_released(vk_left) and not keyboard_check(vk_right)
{
   hspeed = 0;
}
if keyboard_check_released(vk_right) and not keyboard_check(vk_left)
{
   hspeed = 0;
}
if keyboard_check_released(vk_up) and not keyboard_check(vk_down)
{
    vspeed = 0;
}
if keyboard_check_released(vk_down) and not keyboard_check(vk_up)
{
    vspeed = 0;
}



//Sound 

if self.speed > 0 and not audio_is_playing(snd_garb_move_loop){
    audio_play_sound(snd_garb_move_loop, 1, true);
}
if self.speed == 0 and audio_is_playing(snd_garb_move_loop)
    audio_stop_sound(snd_garb_move_loop);
