mp_potential_step(targetx, targety, 3.5, false);

if point_distance(x, y, targetx0, targety0) <= 4 {
    targetx = targetx1;
    targety = targety1;
    }
if point_distance(x, y, targetx1, targety1) <= 2 {
    stopped = true;
    if instance_exists(obj_DDave1_level2) == false {
        global.dave_called = true;
        instance_create(x-250, y-50, obj_DDave1_level2);
        alarm[0] = 6.6  * room_speed;
        targetx = targetx2;
        targety = targety2;
    }
    
}else if point_distance(x, y, targetx2, targety2) <= 4 {
    targetx = targetx3;
    targety = targety3;
    
} else if point_distance(x, y, targetx3, targety3) <= 4 {
    targetx = targetx4;
    targety = targety4;
    
} else if point_distance(x, y, targetx4, targety4) <= 4 {
    instance_destroy();
}
else{
    stopped = false;
}
