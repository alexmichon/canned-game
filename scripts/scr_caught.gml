if caught == true{
    if caught_loop == false and gender == 1{
        audio_play_sound(choose(snd_caught0,snd_caught1,snd_caught2), 80, false);
        caught_loop = true;
    }
    if caught_loop == false and gender == 0{
        audio_play_sound(snd_Fcaught0, 80, false);
        caught_loop = true;
    }
    garbage_detected0 = false;
    garbage_detected1 = false;
    garbage_detected2 = false;
    detected = false;
    stopped = false;
    path_end();
    randomize();
    with (obj_detected)
        instance_destroy();
    
        
    with (obj_caught)
        instance_destroy();
    instance_create(self.x + 30, self.y - 50, obj_caught);
    obj_GARB.speed = 0;
    mp_potential_step(obj_GARB.x, obj_GARB.y, 6.25, false);
    game_timeout += 1;
}

if game_timeout == 2*room_speed{
    if global.lightsOn == true{
        global.notAgain = true;
    }
    room_restart();
}
