if caught == true and not global.sunny_caught{
    if caught_loop == false{
        audio_play_sound(choose(snd_caught0,snd_caught1,snd_caught2), 80, false);
        caught_loop = true;
    }
    garbage_detected0 = false;
    garbage_detected1 = false;
    garbage_detected2 = false;
    stopped = false;
    path_end();
    
    with (obj_detected)
        instance_destroy();
      
    with (obj_caughtDave)
        instance_destroy();
    instance_create(self.x + 30, self.y - 50, obj_caughtDave);
    obj_GARB.speed = 0;
    if not collision_circle(x,y, 25, obj_GARB, 0,1){
        mp_potential_step(obj_GARB.x, obj_GARB.y, 5, false);
    }
    
    else{
        stopped = true;
        scr_dave_dialogue0()
    }

}
