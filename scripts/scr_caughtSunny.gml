if global.sunny_caught{
    with (obj_caught)
        instance_destroy();
    instance_create(self.x + 30, self.y - 50, obj_caught);
    path_end();
    obj_GARB.speed = 0;
    
    if caught_loop == false{
        audio_play_sound(choose(snd_caught0,snd_caught1,snd_caught2), 80, false);
        caught_loop = true;
    }
    if not collision_circle(x,y,25,obj_GARB,0,1){
        stopped = false;
        mp_potential_step(obj_GARB.x, obj_GARB.y, 5, false);
    }
    else{
        stopped = true;
        //dialogue code here
        if not instance_exists(obj_DSunny0_level2){
            instance_create(self.x - 200, y + 55, obj_DSunny0_level2);
        }
    }
}
