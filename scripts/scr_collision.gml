// scr_player_collision()
//Moving down
if(vspeed > 0){
    if(place_meeting(x, y+vspeed, obj_wall)){
        while(!place_meeting(x, y+1, obj_wall)){
            y++;
        }
        vspeed = 0;
    }
}

// Moving up
if(vspeed < 0){
    if(place_meeting(x, y+vspeed, obj_wall)){
        while(!place_meeting(x, y-1, obj_wall)){
            y--;
        }
        vspeed = 0;
    }
}

// Moving right
if(hspeed > 0){
    if(place_meeting(x+hspeed, y, obj_wall)){
        while(!place_meeting(x+1, y, obj_wall)){
            x++;
        }
        hspeed = 0;
    }
}

// Moving left
if(hspeed < 0){
    if(place_meeting(x+hspeed, y, obj_wall)){
        while(!place_meeting(x-1, y, obj_wall)){
            x--;
        }
        hspeed = 0;
    }
}


// for obj_misc
//Moving down
if(vspeed > 0){
    if(place_meeting(x, y+vspeed, obj_misc)){
        while(!place_meeting(x, y+1, obj_misc)){
            y++;
        }
        vspeed = 0;
    }
}

// Moving up
if(vspeed < 0){
    if(place_meeting(x, y+vspeed, obj_misc)){
        while(!place_meeting(x, y-1, obj_misc)){
            y--;
        }
        vspeed = 0;
    }
}

// Moving right
if(hspeed > 0){
    if(place_meeting(x+hspeed, y, obj_misc)){
        while(!place_meeting(x+1, y, obj_misc)){
            x++;
        }
        hspeed = 0;
    }
}

// Moving left
if(hspeed < 0){
    if(place_meeting(x+hspeed, y, obj_misc)){
        while(!place_meeting(x-1, y, obj_misc)){
            x--;
        }
        hspeed = 0;
    }
}

