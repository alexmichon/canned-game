if global.sunny_caught == true{
    with (obj_detected)
        instance_destroy();
    with (obj_caughtDave)
        instance_destroy();
    instance_create(self.x + 30, self.y - 50, obj_caughtDave);
    path_end();
    //dialogue if sunny catches him
    if not collision_circle(x,y,25,obj_GARB,0,1){
        stopped = false;
        mp_potential_step(obj_GARB.x, obj_GARB.y, 5, false);
    }
    else{
        stopped = true;
        // dialogue after he gets to garb.
        if not instance_exists(obj_DDave0_level2){
            global.dave_sunny = true;
            instance_create(self.x - 200, self.y - 55, obj_DDave0_level2);
            obj_DDave0_level2.image_index = 2;
        }
    }
}

else{
    //dialogue if dave catches him
    if not instance_exists(obj_DDave0_level2){
        instance_create(self.x - 200, self.y - 55, obj_DDave0_level2);
    }
}
