// caught 3 times 
if times_detected == 3 and detected == true and caught == false{
    path_end();
    with (obj_detected)
        instance_destroy();
    instance_create(self.x + 30, self.y - 50, obj_caught);
    caught = true;
    detected = false;
}

//detected
if detected == true and caught == false
{   

    if detected_loop == false and gender == 1{
        audio_play_sound(choose(snd_detected0,snd_detected1,snd_detected2), 80, false);
        detected_loop = true;
    }
    if detected_loop == false and gender == 0{
        audio_play_sound(snd_Fdetected0, 80, false);
        detected_loop = true;
    }
    
    if path_pos == false and guard_path != false{
        path_pos = path_position;
        pos_x = self.x;
        pos_y = self.y;
    }
    garbage_detected0 = false;
    garbage_detected1 = false;
    garbage_detected2 = false;
    stopped = false;
    path_end();
    with (obj_detected)
        instance_destroy();        
    instance_create(self.x + 30, self.y - 50, obj_detected);
    mp_potential_step(obj_GARB.x, obj_GARB.y, 4.5, false);
}

// detected but not moving
if collision_circle(x,y, 225, obj_GARB,false,true) and obj_GARB.speed == 0 and detected == true and global.elevator_pressed = false
{
    with (obj_detected)
        instance_destroy();
    detected = false;
    times_detected +=1;
    on_path = false;
}    

//moved out of circle
if (!collision_circle(x,y, 325, obj_GARB,false,true)) and detected == true
{
    with (obj_detected)
        instance_destroy();
    detected = false;
    times_detected +=1;
    on_path = false;
}
        
