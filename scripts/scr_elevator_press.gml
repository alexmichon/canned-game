if collision_circle(obj_elevator.x,obj_elevator.y,75, obj_GARB,false,true){

    with (obj_e)
        instance_destroy();
    instance_create(obj_GARB.x + 30, obj_GARB.y - 50, obj_e);

    if keyboard_check_pressed(ord("E")){
        if instance_exists(obj_door) or instance_exists(obj_doormat)
            audio_play_sound(snd_door, 69, false);
        else
            audio_play_sound(snd_elevator_button, 69, false);
        
        if !instance_exists(obj_caught)
            room_goto_next();
    }
}


else{ 
    if instance_exists(obj_hackBox){
        if not collision_circle(obj_hackBox.x,obj_hackBox.y,75, obj_GARB,false,true) and instance_exists(obj_e){
            with (obj_e)
                instance_destroy();
        }
    }
    
    else if instance_exists(obj_e){
        with (obj_e)
            instance_destroy();
    }
    
}
