if (!collision_line(x,y, obj_GARB.x,obj_GARB.y, obj_wall, false,true))
{
    cone_left = self.direction+sight_cone_left;
    if (cone_left > 359) {cone_left = cone_left - 359};
    cone_right = self.direction+sight_cone_right;
    if (cone_right < 0) {cone_right = cone_right + 359};
    p_angle = point_direction(x,y, obj_GARB.x, obj_GARB.y);
    
    if(cone_right > cone_left)
    {
        if(p_angle <= cone_left or p_angle >= cone_right){ in_cone= true };
        else {in_cone = false};
    }
    else
    {
        if(p_angle <= cone_left and p_angle >= cone_right){ in_cone= true };
        else {in_cone = false};
    }
    
    
    if(in_cone) and caught == false
    {
        
        //caught
        if collision_circle(x,y, 210, obj_GARB,false,true)
        {
            if obj_GARB.speed > 0 or keyboard_check_pressed(vk_space)
                caught = true;
       }
          
       //detected
       if collision_circle(x,y, 325, obj_GARB,false,true)
       {
            if obj_GARB.speed > 0 or keyboard_check_pressed(vk_space)
                detected = true;
       }
       
       if collision_circle(x,y, 275, obj_GARB,false,true)
       {
            //elevator
            if instance_exists(obj_e) and keyboard_check_pressed(ord("E"))
                caught = true;
       }
       
    }
}
//visible = false; this can make enemies invisible behind walls
//visible = true;


