if (!collision_line(x,y, obj_GARB.x,obj_GARB.y, obj_wall, false,true))
{
    cone_left = self.direction+sight_cone_left;
    if (cone_left > 359) {cone_left = cone_left - 359};
    cone_right = self.direction+sight_cone_right;
    if (cone_right < 0) {cone_right = cone_right + 359};
    p_angle = point_direction(x,y, obj_GARB.x, obj_GARB.y);

    if(cone_right > cone_left)
    {
        if(p_angle <= cone_left or p_angle >= cone_right){ in_cone= true };
        else {in_cone = false};
    }
    else
    {
        if(p_angle <= cone_left and p_angle >= cone_right){ in_cone= true };
        else {in_cone = false};
    }


    if(in_cone) and caught == false
    {

        //caught
        if collision_circle(x,y, 325, obj_GARB,false,true)
        {
            global.sunny_caught = true;
        }

    }
}
