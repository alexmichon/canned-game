if garbage_detected1 = true and !instance_exists(obj_garbage1){
    with (obj_detected)
        instance_destroy();
    garbage_detected = false;
    on_path = false;
}

if garbage_detected1 = true and instance_exists(obj_garbage1) and not collision_circle(x,y, 25, obj_garbage1,false,true)
{   
    if detected_loop == false and gender == 1{
        audio_play_sound(choose(snd_detected0,snd_detected1,snd_detected2), 80, false);
        detected_loop = true;
    }
    if detected_loop == false and gender == 0{
        audio_play_sound(snd_Fdetected0, 80, false);
        detected_loop = true;
    }
    if path_pos == false and guard_path != false{
        path_pos = path_position;
        pos_x = self.x;
        pos_y = self.y;
    }
    path_end();
    with (obj_detected)
        instance_destroy();         
    instance_create(self.x + 30, self.y - 50, obj_detected);
    mp_potential_step(obj_garbage1.x, obj_garbage1.y, 4.5, false);
    stopped = false;
}

if collision_circle(x,y, 25, obj_garbage1, 0, 1) and garbage_detected1 == true
{
    with (obj_garbage1)
        instance_destroy();
    with (obj_detected)
        instance_destroy();
    instance_create(self.x + 30, self.y - 50, obj_detected);
    garbage_detected1 = false;
    stopped = true;
    
    alarm[0] = room_speed *5;
}

