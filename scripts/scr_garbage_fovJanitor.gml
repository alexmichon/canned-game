//garbage0
if instance_exists(obj_garbage0) and not caught{
    if (!collision_line(x,y, obj_garbage0.x,obj_garbage0.y, obj_wall, false,true))
    {
        cone_left_garbage = self.direction+sight_cone_left;
        if (cone_left_garbage > 359) {cone_left_garbage = cone_left_garbage - 359};
        cone_right_garbage = self.direction+sight_cone_right;
        if (cone_right_garbage < 0) {cone_right_garbage = cone_right_garbage + 359};
        g_angle = point_direction(x,y, obj_garbage0.x, obj_garbage0.y);
        
        if(cone_right_garbage > cone_left_garbage)
        {
            if(g_angle <= cone_left_garbage or g_angle >= cone_right_garbage){ garbage_in_cone= true };
            else {garbage_in_cone = false};
        }
        else
        {
            if(g_angle <= cone_left_garbage and g_angle >= cone_right_garbage){ garbage_in_cone= true };
            else {garbage_in_cone = false};
        }
        
        
        if(garbage_in_cone)
        {
            if collision_circle(x,y, 325, obj_garbage0,false,true){
                garbage_detected0 = true;
            }
        }
    }
    if collision_circle(x,y, 175, obj_garbage0, 0,1){
        garbage_detected0 = true;
    }
}

//garbage1
if instance_exists(obj_garbage1) and not caught{
    if (!collision_line(x,y, obj_garbage1.x,obj_garbage1.y, obj_wall, false,true))
    {
        cone_left_garbage = self.direction+sight_cone_left;
        if (cone_left_garbage > 359) {cone_left_garbage = cone_left_garbage - 359};
        cone_right_garbage = self.direction+sight_cone_right;
        if (cone_right_garbage < 0) {cone_right_garbage = cone_right_garbage + 359};
        g_angle = point_direction(x,y, obj_garbage1.x, obj_garbage1.y);
        
        if(cone_right_garbage > cone_left_garbage)
        {
            if(g_angle <= cone_left_garbage or g_angle >= cone_right_garbage){ garbage_in_cone= true };
            else {garbage_in_cone = false};
        }
        else
        {
            if(g_angle <= cone_left_garbage and g_angle >= cone_right_garbage){ garbage_in_cone= true };
            else {garbage_in_cone = false};
        }
        
        
        if(garbage_in_cone)
        {
            if collision_circle(x,y, 325, obj_garbage1,false,true){
                garbage_detected1 = true;
            }
        }
    }
    
    if collision_circle(x,y, 175, obj_garbage1, 0,1){
        garbage_detected1 = true;
    }
}
//garbage2
if instance_exists(obj_garbage2) and not caught{
    if (!collision_line(x,y, obj_garbage2.x,obj_garbage2.y, obj_wall, false,true))
    {
        cone_left_garbage = self.direction+sight_cone_left;
        if (cone_left_garbage > 359) {cone_left_garbage = cone_left_garbage - 359};
        cone_right_garbage = self.direction+sight_cone_right;
        if (cone_right_garbage < 0) {cone_right_garbage = cone_right_garbage + 359};
        g_angle = point_direction(x,y, obj_garbage2.x, obj_garbage2.y);
        
        if(cone_right_garbage > cone_left_garbage)
        {
            if(g_angle <= cone_left_garbage or g_angle >= cone_right_garbage){ garbage_in_cone= true };
            else {garbage_in_cone = false};
        }
        else
        {
            if(g_angle <= cone_left_garbage and g_angle >= cone_right_garbage){ garbage_in_cone= true };
            else {garbage_in_cone = false};
        }
        
        
        if(garbage_in_cone)
        {
            if collision_circle(x,y, 325, obj_garbage2,false,true){
                garbage_detected2 = true;
            }
        }
    }
    if collision_circle(x,y, 175, obj_garbage2, 0,1){
        garbage_detected2 = true;
    }
}
