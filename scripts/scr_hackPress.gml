if global.lightsOn == false{
    if collision_circle(obj_hackBox.x,obj_hackBox.y,75, obj_GARB,false,true){
    
        with (obj_e){
            instance_destroy();
        }
    
        instance_create(obj_GARB.x + 30, obj_GARB.y - 50, obj_e);
        
        
        if keyboard_check_pressed(ord("E")){
                instance_create(-100,-100,obj_miniGameManager);
        }
    }
    
    else if not collision_circle(obj_elevator.x,obj_elevator.y,75, obj_GARB,false,true){
        with (obj_e){
            instance_destroy();
        }
    }
}
