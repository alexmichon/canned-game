//--------------------Level 1------------------------------
if (room_get_name(room) == "room_tutorial"){
    obj_miniGame.image_index = 0;
    

    global.array_answer[ 0] = "blank";
    global.array_answer[ 1] = "blank";
    global.array_answer[ 2] = "terminalR3";
    global.array_answer[ 3] = "terminalG3";
    global.array_answer[ 4] = "blank";
    
    global.array_answer[ 5] = "blank";
    global.array_answer[ 6] = "blank";
    global.array_answer[ 7] = "red0";
    global.array_answer[ 8] = "green0";
    global.array_answer[ 9] = "blank";
    
    global.array_answer[10] = "blank";
    global.array_answer[11] = "green5";
    global.array_answer[12] = "blank";
    global.array_answer[13] = "green2";
    global.array_answer[14] = "blank";
    
    global.array_answer[15] = "blank";
    global.array_answer[16] = "green0";
    global.array_answer[17] = "red0";
    global.array_answer[18] = "blank";
    global.array_answer[19] = "blank";
    
    global.array_answer[20] = "blank";
    global.array_answer[21] = "terminalG1";
    global.array_answer[22] = "terminalR1";
    global.array_answer[23] = "blank";
    global.array_answer[24] = "blank";
}
//--------------------Level 2------------------------------
if (room_get_name(room) == "room_level3_room1"){
    obj_miniGame.image_index = 1;

    global.array_answer[ 0] = "terminalB0";
    global.array_answer[ 1] = "blue1";
    global.array_answer[ 2] = "blue1";
    global.array_answer[ 3] = "blue1";
    global.array_answer[ 4] = "blue4";
    
    global.array_answer[ 5] = "red5";
    global.array_answer[ 6] = "terminalR2";
    global.array_answer[ 7] = "terminalG0";
    global.array_answer[ 8] = "green4";
    global.array_answer[ 9] = "terminalB1";
    
    global.array_answer[10] = "red0";
    global.array_answer[11] = "terminalG3";
    global.array_answer[12] = "terminalR0";
    global.array_answer[13] = "blank";
    global.array_answer[14] = "red4";
    
    global.array_answer[15] = "red0";
    global.array_answer[16] = "green3";
    global.array_answer[17] = "green1";
    global.array_answer[18] = "green2";
    global.array_answer[19] = "red0";
    
    global.array_answer[20] = "red3";
    global.array_answer[21] = "red1";
    global.array_answer[22] = "red1";
    global.array_answer[23] = "red1";
    global.array_answer[24] = "red2";
}
//--------------------Level 3------------------------------
if (room_get_name(room) == "room_level2_room1"){
    obj_miniGame.image_index = 2;
    

    global.array_answer[ 0] = "terminalB0";
    global.array_answer[ 1] = "blue1";
    global.array_answer[ 2] = "blue1";
    global.array_answer[ 3] = "blue1";
    global.array_answer[ 4] = "blue4";
    
    global.array_answer[ 5] = "terminalG0";
    global.array_answer[ 6] = "green1";
    global.array_answer[ 7] = "green1";
    global.array_answer[ 8] = "green4";
    global.array_answer[ 9] = "blue0";
    
    global.array_answer[10] = "terminalR3";
    global.array_answer[11] = "terminalB0";
    global.array_answer[12] = "blue4";
    global.array_answer[13] = "green0";
    global.array_answer[14] = "blue0";
    
    global.array_answer[15] = "red0";
    global.array_answer[16] = "terminalG0";
    global.array_answer[17] = "blank";
    global.array_answer[18] = "green2";
    global.array_answer[19] = "blue0";
    
    global.array_answer[20] = "red3";
    global.array_answer[21] = "terminalR2";
    global.array_answer[22] = "blue3";
    global.array_answer[23] = "blue1";
    global.array_answer[24] = "blue2";
}
//--------------------Level 4------------------------------
if (room_get_name(room) == "room_level1_room2"){
    obj_miniGame.image_index = 3;
    

    global.array_answer[ 0] = "blue5";
    global.array_answer[ 1] = "blue1";
    global.array_answer[ 2] = "blue4";
    global.array_answer[ 3] = "red5";
    global.array_answer[ 4] = "terminalR2";
    
    global.array_answer[ 5] = "blue0";
    global.array_answer[ 6] = "red5";
    global.array_answer[ 7] = "blank";
    global.array_answer[ 8] = "red2";
    global.array_answer[ 9] = "terminalG3";
    
    global.array_answer[10] = "blue0";
    global.array_answer[11] = "red0";
    global.array_answer[12] = "terminalB1";
    global.array_answer[13] = "terminalG0";
    global.array_answer[14] = "green2";
    
    global.array_answer[15] = "blue0";
    global.array_answer[16] = "red3";
    global.array_answer[17] = "red1";
    global.array_answer[18] = "terminalR2";
    global.array_answer[19] = "terminalB3";
    
    global.array_answer[20] = "blue3";
    global.array_answer[21] = "blue1";
    global.array_answer[22] = "blue1";
    global.array_answer[23] = "blue1";
    global.array_answer[24] = "blue2";
}
//--------------------Level 5------------------------------
/*if (room_get_name(room) == "room_level1_room1"){
    obj_miniGame.image_index = 4;
    

    global.array_answer[ 0] = "terminalR0";
    global.array_answer[ 1] = "red1";
    global.array_answer[ 2] = "red1";
    global.array_answer[ 3] = "red1";
    global.array_answer[ 4] = "red4";
    
    global.array_answer[ 5] = "blue5";
    global.array_answer[ 6] = "blue1";
    global.array_answer[ 7] = "blue1";
    global.array_answer[ 8] = "blue4";
    global.array_answer[ 9] = "red0";
    
    global.array_answer[10] = "terminalB1";
    global.array_answer[11] = "red5";
    global.array_answer[12] = "red1";
    global.array_answer[13] = "blank";
    global.array_answer[14] = "red2";
    
    global.array_answer[15] = "green5";
    global.array_answer[16] = "blank";
    global.array_answer[17] = "terminalG2";
    global.array_answer[18] = "blue3";
    global.array_answer[19] = "terminalB2";
    
    global.array_answer[20] = "terminalG1";
    global.array_answer[21] = "red3";
    global.array_answer[22] = "red1";
    global.array_answer[23] = "red1";
    global.array_answer[24] = "terminalR2";
}*/
