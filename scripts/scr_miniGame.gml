scr_miniGameTutorial();

// move right
if keyboard_check_pressed(vk_right){
    thisx = thisx + global.block_width;
    i = i + 1;
    if thisx > (view_wview[0]/2)+(2*global.block_width){
        thisx = thisx - 5*global.block_width;
        i = i - 5;
    }
    with (obj_highlight){
        instance_destroy();
    }
    instance_create(thisx,thisy,obj_highlight);
    audio_play_sound(snd_miniMove,0,false);
}
//move left
if keyboard_check_pressed(vk_left){
    thisx = thisx - global.block_width;
    i = i - 1;
    if thisx < (view_wview[0]/2)-(2*global.block_width){
        thisx = thisx + 5*global.block_width;
        i = i + 5;
    }
    with (obj_highlight){
        instance_destroy();
    }
    instance_create(thisx,thisy,obj_highlight);
    audio_play_sound(snd_miniMove,0,false);
}
//move up
if keyboard_check_pressed(vk_up){
    thisy = thisy - global.block_width;
    i = i - 5;
    if thisy < (view_hview[0]/2)-(2*global.block_width){
        thisy = thisy + 5*global.block_width;
        i = i + 25;
    }
    with (obj_highlight){
        instance_destroy();
    }
    instance_create(thisx,thisy,obj_highlight);
    audio_play_sound(snd_miniMove,0,false);
}
//move down
if keyboard_check_pressed(vk_down){
    thisy = thisy + global.block_width;
    i = i + 5;
    if thisy > (view_hview[0]/2)+(2*global.block_width){
        thisy = thisy - 5*global.block_width;
        i = i - 25;
    }
    with (obj_highlight){
        instance_destroy();
    }
    instance_create(thisx,thisy,obj_highlight);
    audio_play_sound(snd_miniMove,0,false);
}

if (global.array_answer[i] != "blank"){
    // red block
    if keyboard_check_pressed(ord("R")){
        result = false;
        if (string_char_at(global.array_answer[i],0) == "t"){
                var inst = instance_position(thisx,thisy,obj_redterminal);
                if inst{
                    if (inst.image_index == 0) and (result == false){
                        inst.image_index = 1;
                        global.array[i] = "terminalR1";
                        result = true;
                    }
                    if (inst.image_index == 1) and (result == false){
                        inst.image_index = 2;
                        global.array[i] = "terminalR2";
                        result = true;
                    }
                    if (inst.image_index == 2) and (result == false){
                        inst.image_index = 3;
                        global.array[i] = "terminalR3";
                        result = true;
                    }
                    if (inst.image_index == 3) and (result == false){
                        inst.image_index = 0;
                        global.array[i] = "terminalR0";
                        result = true;
                    }
                    audio_play_sound(snd_miniTermRotate,0,false);
                }
        
            else{
                var inst = instance_position(thisx,thisy,obj_Block);
                if inst{
                    with inst{
                        instance_destroy();
                    }
                }
                instance_create(thisx,thisy,obj_redterminal);
                global.array[i] = "terminalR0";
                audio_play_sound(snd_miniTermPlace,0,false);
            }
        }
        else{
            var inst = instance_position(thisx,thisy,obj_RedBlock);
            if inst{
                if (inst.image_index == 0) and (result == false){
                    inst.image_index = 1;
                    global.array[i] = "red1";
                    result = true;
                }
                if (inst.image_index == 1) and (result == false){
                    inst.image_index = 2;
                    global.array[i] = "red2";
                    result = true;
                }
                if (inst.image_index == 2) and (result == false){
                    inst.image_index = 3;
                    global.array[i] = "red3";
                    result = true;
                }
                if (inst.image_index == 3) and (result == false){
                    inst.image_index = 4;
                    global.array[i] = "red4";
                    result = true;
                }
                if (inst.image_index == 4) and (result == false){
                    inst.image_index = 5;
                    global.array[i] = "red5";
                    result = true;
                }
                if (inst.image_index == 5) and (result == false){
                    inst.image_index = 0;
                    global.array[i] = "red0";
                    result = true;
                }
                audio_play_sound(snd_miniRotate,0,false);
            }
        
            else{
                var inst = instance_position(thisx,thisy,obj_Block);
                if inst{
                    with inst{
                        instance_destroy();
                    }
                }
                instance_create(thisx,thisy,obj_RedBlock);
                global.array[i] = "red0";
                audio_play_sound(snd_miniPlace,0,false);
            }
        }
    }
    
    //blue block
    if keyboard_check_pressed(ord("B")){
        result = false;
        if (string_char_at(global.array_answer[i],0) == "t"){
                var inst = instance_position(thisx,thisy,obj_blueterminal);
                if inst{
                    if (inst.image_index == 0) and (result == false){
                        inst.image_index = 1;
                        global.array[i] = "terminalB1";
                        result = true;
                    }
                    if (inst.image_index == 1) and (result == false){
                        inst.image_index = 2;
                        global.array[i] = "terminalB2";
                        result = true;
                    }
                    if (inst.image_index == 2) and (result == false){
                        inst.image_index = 3;
                        global.array[i] = "terminalB3";
                        result = true;
                    }
                    if (inst.image_index == 3) and (result == false){
                        inst.image_index = 0;
                        global.array[i] = "terminalB0";
                        result = true;
                    }
                    audio_play_sound(snd_miniTermRotate,0,false);
                }
        
            else{
                var inst = instance_position(thisx,thisy,obj_Block);
                if inst{
                    with inst{
                        instance_destroy();
                    }
                }
                instance_create(thisx,thisy,obj_blueterminal);
                global.array[i] = "terminalB0";
                audio_play_sound(snd_miniTermPlace,0,false);
            }
        }
        else{
            var inst = instance_position(thisx,thisy,obj_BlueBlock);
            if inst{
                if (inst.image_index == 0) and (result == false){
                    inst.image_index = 1;
                    global.array[i] = "blue1";
                    result = true;
                }
                if (inst.image_index == 1) and (result == false){
                    inst.image_index = 2;
                    global.array[i] = "blue2";
                    result = true;
                }
                if (inst.image_index == 2) and (result == false){
                    inst.image_index = 3;
                    global.array[i] = "blue3";
                    result = true;
                }
                if (inst.image_index == 3) and (result == false){
                    inst.image_index = 4;
                    global.array[i] = "blue4";
                    result = true;
                }
                if (inst.image_index == 4) and (result == false){
                    inst.image_index = 5;
                    global.array[i] = "blue5";
                    result = true;
                }
                if (inst.image_index == 5) and (result == false){
                    inst.image_index = 0;
                    global.array[i] = "blue0";
                    result = true;
                }
                audio_play_sound(snd_miniRotate,0,false);
            }
        
            else{
                var inst = instance_position(thisx,thisy,obj_Block);
                if inst{
                    with inst{
                        instance_destroy();
                    }
                }
                instance_create(thisx,thisy,obj_BlueBlock);
                global.array[i] = "blue0";
                audio_play_sound(snd_miniPlace,0,false);
            }
        }
    }
    //green block
    if keyboard_check_pressed(ord("G")){
    
        result = false;
        if (string_char_at(global.array_answer[i],0) == "t"){
                var inst = instance_position(thisx,thisy,obj_greenterminal);
                if inst{
                    if (inst.image_index == 0) and (result == false){
                        inst.image_index = 1;
                        global.array[i] = "terminalG1";
                        result = true;
                    }
                    if (inst.image_index == 1) and (result == false){
                        inst.image_index = 2;
                        global.array[i] = "terminalG2";
                        result = true;
                    }
                    if (inst.image_index == 2) and (result == false){
                        inst.image_index = 3;
                        global.array[i] = "terminalG3";
                        result = true;
                    }
                    if (inst.image_index == 3) and (result == false){
                        inst.image_index = 0;
                        global.array[i] = "terminalG0";
                        result = true;
                    }
                    audio_play_sound(snd_miniTermRotate,0,false);
                }
        
            else{
                var inst = instance_position(thisx,thisy,obj_Block);
                if inst{
                    with inst{
                        instance_destroy();
                    }
                }
                instance_create(thisx,thisy,obj_greenterminal);
                global.array[i] = "terminalG0";
                audio_play_sound(snd_miniTermPlace,0,false);
            }
        }
        else{
            var inst = instance_position(thisx,thisy,obj_GreenBlock);
            if inst{
                if (inst.image_index == 0) and (result == false){
                    inst.image_index = 1;
                    global.array[i] = "green1";
                    result = true;
                }
                if (inst.image_index == 1) and (result == false){
                    inst.image_index = 2;
                    global.array[i] = "green2";
                    result = true;
                }
                if (inst.image_index == 2) and (result == false){
                    inst.image_index = 3;
                    global.array[i] = "green3";
                    result = true;
                }
                if (inst.image_index == 3) and (result == false){
                    inst.image_index = 4;
                    global.array[i] = "green4";
                    result = true;
                }
                if (inst.image_index == 4) and (result == false){
                    inst.image_index = 5;
                    global.array[i] = "green5";
                    result = true;
                }
                if (inst.image_index == 5) and (result == false){
                    inst.image_index = 0;
                    global.array[i] = "green0";
                    result = true;
                }
                audio_play_sound(snd_miniRotate,0,false);
            }
        
            else{
                var inst = instance_position(thisx,thisy,obj_Block);
                if inst{
                    with inst{
                        instance_destroy();
                    }
                }
                instance_create(thisx,thisy,obj_GreenBlock);
                global.array[i] = "green0";
                audio_play_sound(snd_miniPlace,0,false);
            }
        }
    }
}
else{
    if (room_get_name(room) == "room_tutorial"){
        global.txt2 = "You do not need to use this space in the tutorial";
        draw_text(view_wview[0]/2,(view_hview[0]/2)+425, global.txt2);
    }
}

//clear_block
if keyboard_check_pressed(ord("C")){
    var inst = instance_position(thisx,thisy,obj_Block);
    if inst{
        with inst{
            instance_destroy();
        }
    }
    global.array[i] = "blank";
}

//check solution
if keyboard_check_pressed(vk_enter){
    arrayEquals = true;
    
    for (w = 0; w < 25; w++){    
        if (global.array[w] != global.array_answer[w]){
            arrayEquals = false;
        }
    }
    if arrayEquals == true{
        surface_free(miniGameSurf);
        
        with (obj_miniGame){
            instance_destroy();
        }
        with (obj_Block){
            instance_destroy();
        }
        with (obj_highlight){
            instance_destroy();
        }
        instance_activate_all();
        audio_play_sound(snd_miniWin,0,false);
        obj_GARB.speed = 0;
        if (obj_GARB.dir == 0){
            obj_GARB.image_index = 0;
        }
        else if (obj_GARB.dir == 90){
             obj_GARB.image_index = 1;
        }
        else if (obj_GARB.dir == 180){
             obj_GARB.image_index = 2;
        }
        else if (obj_GARB.dir == 270){
             obj_GARB.image_index = 3;
        }
        
        scr_GARB_outline();
        draw_set_font(fnt_arial);
        instance_destroy();
        window_set_cursor(cr_none);
        
        with (obj_GUI_ex){
            instance_destroy();
        }
        
        global.lightsOn = true;
        global.notAgain = true;
    }
}
