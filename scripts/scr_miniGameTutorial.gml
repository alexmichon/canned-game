if (room_get_name(room) == "room_tutorial"){
    //print instructions as the player progresses
    draw_text(view_wview[0]/2,(view_hview[0]/2)-425, global.txt);
    
    if keyboard_check_pressed(vk_anykey){
        with (obj_GUI_ex){
            instance_destroy();
        }
        global.txt2 = "";
    }
    
    
    

    arrayEquals = true;
    for (w = 0; w < 25; w++){    
        if (global.array[w] != global.array_answer[w]){
            arrayEquals = false;
        }
    }
    if arrayEquals == true{
        global.txt = "Press 'Enter' to submit solution"
        instance_create(view_wview[0]/2-200,(view_hview[0]/2)-425,obj_GUI_ex);
    }

    if (step == 7) and (keyboard_check_released(ord("B")) or keyboard_check_released(ord("G"))){
            global.txt = "Complete the Green wire connection to finish";
            instance_create(view_wview[0]/2-300,(view_hview[0]/2)-425,obj_GUI_ex);
            step += 1;
    }
    if (step == 6) and (global.array[7] == "red0") and (global.array[17] == "red0"){
            global.txt = "'B' places a BLUE wire and 'G' places a GREEN wire";
            instance_create(view_wview[0]/2-3025,(view_hview[0]/2)-425,obj_GUI_ex);
            step += 1;
    }
    if (step == 5) and (global.array[2] == "terminalR3") and (global.array[22] == "terminalR1"){
            global.txt = "Complete the red wire connection!";
            instance_create(view_wview[0]/2-250,(view_hview[0]/2)-425,obj_GUI_ex);
            step += 1;
    }
    if (step == 4) and (global.array[i] == "terminalR0"){
        global.txt = "Place an upwards terminal wire on the bottom Red terminal#and a downwards terminal wire on the top Red terminal";
        instance_create(view_wview[0]/2-425,(view_hview[0]/2)-425,obj_GUI_ex);
        step += 1;
    }
    if (step == 3) and keyboard_check_released(ord("C")){
            global.txt = "Press 'R' over a terminal to produce a terminal node";
            instance_create(view_wview[0]/2-325,(view_hview[0]/2)-425,obj_GUI_ex);
            step += 1;
    }
    if (step == 2)and keyboard_check_released(ord("R")){
           global.txt = "Press 'C' to clear wire";
           instance_create(view_wview[0]/2-175,(view_hview[0]/2)-425,obj_GUI_ex);
           step += 1;
    }
    if (step == 1) and keyboard_check_released(ord("R")){
            newx = thisx;
            newy = thisy;
           global.txt = "Press 'R' again to change wire orientation";
           instance_create(view_wview[0]/2-275,(view_hview[0]/2)-425,obj_GUI_ex);
           step += 1;
    }
    if step == 0{
    
        for (w = 0; w < 25; w++){
            global.array_answer[w] = "null";
        }
        if keyboard_check_released(vk_right) or keyboard_check_released(vk_left) 
                                            or keyboard_check_released(vk_up)
                                            or keyboard_check_released(vk_down){
               global.txt = "Press 'R' to place a RED wire";
               instance_create(view_wview[0]/2-200,(view_hview[0]/2)-425,obj_GUI_ex)
               step += 1;
        }
        scr_makeSol();
        with (obj_Block){
            instance_destroy();
        }
        scr_makeSol();
    }
}
//other rooms instructions
else{
    //print instructions
    draw_text(view_wview[0]/2,(view_hview[0]/2)+400, "Use 'R', 'G', 'B' to cycle through different wires#Use arrow keys to change place on grid");
    draw_text(view_wview[0]/2,(view_hview[0]/2)-400, "Connect wires to complete hack#Press 'Esc' to resume");
}
