//return to path
if on_path == false and caught == false{
    if garbage_detected0 == true {
        on_path = true;
    }
    if garbage_detected1 == true {
        on_path = true;
    }
    if garbage_detected2 == true {
        on_path = true;
    }
    detected_loop = false;
    
    mp_potential_step(pos_x, pos_y, 3.5, false); 
    stopped = false;   
       
    if self.x == pos_x  and self.y == pos_y{
        if guard_path != false{
            path_start(guard_path, 3.5, path_action_restart, true);
            direction = dir;
            path_position = path_pos;
            path_pos = false;
            on_path = true;
        }
        else {
            on_path = true;
            direction = dir;
            stopped = true;
        }
    }
}
