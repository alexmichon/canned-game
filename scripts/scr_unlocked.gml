if (room_get_name(room) == "room_tutorial_test") and (global.unlocked == 0){
    global.unlocked = 1;
}
if (room_get_name(room) == "room_tutorial") and (global.unlocked == 1){
    global.unlocked = 2;
}
if (room_get_name(room) == "room_level3_room1") and (global.unlocked == 2){
    global.unlocked = 3;
}
if (room_get_name(room) == "room_level3_room2") and (global.unlocked == 3){
    global.unlocked = 4;
}
if (room_get_name(room) == "room_level2_room1") and (global.unlocked == 4){
    global.unlocked = 5;
}
if (room_get_name(room) == "room_level2_room2") and (global.unlocked == 5){
    global.unlocked = 6;
}
if (room_get_name(room) == "room_level1_room2") and (global.unlocked == 6){
    global.unlocked = 7;
}
if (room_get_name(room) == "room_level1_room1") and (global.unlocked == 7){
    global.unlocked = 8;
}

ini_open("savedata.ini");
ini_write_real("save","level",global.unlocked);
ini_close();
